#Det här programmet skall:
# 1. Rensa en text så att det kan användas som corpus.
# 2. Sortera ut alla ord och dess frekvenser.
# 3. Räkna förekomst av bokstäver och bokstavskombinationer (chars[xy])

# Scriptet letar reda på alla filer som slutar med .txt

# Programmet skriver till följande filer i mappen result:
# theCleanedText.txt : Texten när den är städad
# allWordsAndTheirFrequency.txt : alla ord som finns i texten och deras frekvens
# letterFrequency.txt : alla bokstäver som finns i texten och deras frekvens
# charPairFrequency.txt : alla bokstavspar som finns i texten och deras frekvens


@files = <*.txt>;
foreach $file (@files) {
	open(FILE, "$file") || die "Could not open file $file.";
	print $file;
	$text = <>;

# Reads the lines in the file and removes some of those who don't belong to the text.
	while ($line = <FILE>) {
		if(($line !~/^(Email|To|Till|Prioritet|Sent|URL|From|Date|Subject|References|Message-ID|In-Reply-To|Fr�n|�mne|Skickat):*/) and ($line !~ /^\>/) and ($line !~ /^(_|--)+/) and !($line =~ /^$/))  # Kolla att line inte bÃ¶rjar med en >, dÃ¥ skall det inte med.# Kolla att line inte bÃ¶rjar med From, Date, Subject, References  eller Message-ID, dÃ¥ skall den inte heller med
		{
			$text.= $line;
		}
	}
	open (FILE , '>Result/CleanedText'.$file);
	print FILE $text ;
	close(FILE);
#StÃ¤dar texten
	$text =~ tr/A-Z���/a-z���/;#byter ut versaler mot gemena
	$text =~ tr/[0-2][0-9]:[0-5][0-9]//d;#ta bort klockslag
#	$text =~ tr/^[a-z]\.[a-z]+?\.[a-z]{2,4}$//d;#tar bort webadresser
		#^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$
	$text =~ s/\// /g; #byter ut alla / mot blanksteg
	$text =~ s/[.]/ /g; #byter ut alla . mot blanksteg
	$text =~ s/[_]/ /g; #byter ut alla _ mot blanksteg
	$text =~ s/[&]/ /g; #byter ut alla & mot blanksteg
	$text =~ s/[:]/ /g; #byter ut alla : mot blanksteg
	$text =~ s/[+]/ /g; #byter ut alla + mot blanksteg
	$text =~ s/-/ /g; #byter ut alla - mot blanksteg
	$text =~ s/\n/ /g; #byter ut alla nyrad mot blanksteg
	$text =~ s/\t/ /g; #byter ut alla tab mot blanksteg
	$text =~ s/\f/ /g; #byter ut alla linefeed mot blanksteg
	$text =~ tr/[#$,%!?<>�;�"^~�`'\(\)\*\\]//d;#tar bort all kommatering
	$text =~ tr/[\W]//d;
	#$text =~ tr///d;
	#$text =~ tr///d;
	#$text =~ tr///d;
	$text =~ s/[ ]+/\n/g;#byter ut blanksteg mot nyrad
#LÃ¤gger in alla ord i en lista
		@words = split(/\n/, $text);

#Tar ut alla unika ord och deras frekvens
	for ($i = 0; $i <= $#words; $i++) {
		if(($words[$i]=~/[:alpha:]+/) && ($words[$i]!~/(@|=)/) ){
			$frequency{$words[$i]}++;
		}
	}
}

#Utskrifter!
open (FILE , '>Result/allWordsAndTheirFrequency.txt');
foreach $key (sort (keys(%frequency))){
	print FILE $key.": ".$frequency{$key}. "\n" ;
}

close(FILE);
#Ta ut alla bokstavspar och deras frekvens och skriver till filen chars.txt .

foreach $key (keys(%frequency)){
	while ($key =~ /(..)/g) {
		$chcount{$1}+=$frequency{$key};
	}
}
open (FILE , '>Result/charPairFrequency.txt');
foreach $key (sort (keys(%chcount))){
	print FILE $key.": ".$chcount{$key}. "\n" ;
}

close(FILE);
foreach $key (keys(%frequency)){
	while ($key =~ /(.)/g) {
		$letter{$1}+=$frequency{$key};
	}
}
open (FILE , '>Result/letterFrequency.txt');
foreach $key (sort (keys(%letter))){
	print FILE $key.": ".$letter{$key}. "\n" ;
}
