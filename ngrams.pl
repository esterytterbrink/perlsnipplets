($file_name, $n) = @ARGV;
open(FILE, "$file_name") || die "Could not open file $file_name.";
$n = @ARGV;
while ($line = <FILE>) {
   $text .= $line;
}
# The next line is a very primitive tokenizer
$text =~ tr/a-zåàâäæçéèêëîïôöœßùûüÿA-ZÅÀÂÄÆÇÉÈÊËÎÏÔÖŒÙÛÜ/\n/cs;

@words = split(/\n/, $text);
for ($i = 0; $i < $#words; $i++) {
	$tmp = $words[$i];
	for($j =1; $j<$n; $j++){
		$tmp .= " " . $words[($i + $j)];
	}
	$bigrams[$i] = $tmp;
}
for ($i = 0; $i < $#bigrams; $i++) {
	if (!exists($frequency_bigrams{$bigrams[$i]})) {
		$frequency_bigrams{$bigrams[$i]} = 1;
	} else {
		$frequency_bigrams{$bigrams[$i]}++;
	}
}
foreach $bigram (sort keys %frequency_bigrams){
	print "$frequency_bigrams{$bigram} $bigram \n";
}

print scalar($frequency_bigrams); 
